+++
title = "About"
draft = false
hidden = true
+++

Ich bin Software Engineer in München und arbeite mit unterschiedlichen Technologien. Mein Schwerpunkt liegt in der Entwicklung von Softwarelösungen mithilfe von Spring, Angular React, Bash. Ich bin stets auf der Suche nach neuen Konzepten und Vorgehen in der Entwicklung von Software mit Java und JavaScript. Zudem bin ich häufig auf Meetups rund um Softwareentwicklung im Raum München anzutreffen.

## Vorträge

- Java Forum Nord (September 2019, Hannover) - OOP meets FP und warum ich Java trotzdem mag - [Google Slides](https://docs.google.com/presentation/d/14vFXcQ3CqetFqm24qc7BpB5JjGw7G6i4ZdIAwR-pYMM "Folien vom Talk als Google SLides") [Beispiel-Code](https://gitlab.com/torstenkohn/2019-java-forum-nord-oop-meets-fp "Beispiel-Code bei GitLab")
- Java Forum Stuttgart (Juli 2018) - Reaktive Programmierung mit Spring Boot und Project Reactor - [Google Slides](https://docs.google.com/presentation/d/1PU0rCu-qXb4r8IXRyoLpwTQhSltnWwtL2gLxYw9Clv0/edit?usp=sharing "Folien vom Talk als Google Slides") [Beispiel-Code](https://gitlab.com/torstenkohn/reactive-java-forum-stuttgart-demo "Demoprojekt zum Vortrag")
- Lightweight Java User Group (Juni 2018, München) - Reaktive Programmierung mit Spring Boot und Project Reactor - [Google Slides](https://docs.google.com/presentation/d/1Xo87rbHnfaiLRsVOtm-f5chBrxykMVtvoL12Y9iaOHg/edit?usp=sharing "Folien vom Meetup als Google Slides") [Beispiel-Code](https://gitlab.com/torstenkohn/reactive-ljug-demo "Demoprojekt zum Vortrag")
- Herbstcampus (September 2017) - Reaktive Programmierung mit Java und Spring 5

## Veröffentlichungen

- Java aktuell 03/2018 - Reaktive Programmierung mit Java und Spring [PDF](/downloads/2018_Java_aktuell_Reaktive-Programmierung-mit-Java-und-Spring.pdf"Artikel als PDF Download")

## Qualifikationen

- iSAQB Certified Professional for Software Architecture Foundation Level - 2019
- Professional Scrum Product Owner (PSPO I) - 2019
- Professional Scrum Master (PSM I) - 2017
- Spring Professional v4.3 - 2017
- Angular 2 & TypeScript Grundlagen (Schulung bei thecodecampus 2017)

## Ausbildung

- B. Sc. Medieninformatik (Hochschule Düsseldorf, September 2011 - Juli 2015)
- Mechatroniker (PM°DM, Villingen-Schwenningen, September 2005 - Juni 2008)
- IT-Systemelektroniker (Telekom, Kiel, September 2002 - Juni 2005)
