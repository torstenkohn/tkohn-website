+++
title = "Impressum"
description = ""
date = "2018-06-30T21:39:59+02:00"
draft = false
hidden = true
+++

<p>Torsten Kohn<br />
Luisenstr. 60A<br />
80798 München
</p>

<p>
Telefon: +49 160 93 845 149<br />
E-Mail: torsten+kohn@kohn.dev
</p>

<p>
Verantwortlich für den Inhalt gem. § 55 Abs. 2 RStV:<br />
Torsten Kohn<br />
Nymphenburger Str. 49<br />
80335 München<br />
</p>

<p>Wir nehmen nicht an einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teil.</p>

<p><a href="https://www.123recht.net/impressumsgenerator.asp">Impressum erstellt mit dem Impressumsgenerator von 123recht.net - Rechtsberatung online.</a></p>
