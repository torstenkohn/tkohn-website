# install wget to download hugo binaries
yum install -y wget

# download the theme and move it to themes/kiss
wget https://github.com/ribice/kiss/archive/v2.0.0.tar.gz
tar -xzf v2.0.0.tar.gz
mkdir themes
mv kiss-2.0.0 themes/kiss

# download the binaries of hugo to generate the static files
wget https://github.com/gohugoio/hugo/releases/download/v0.55.2/hugo_0.55.2_Linux-64bit.tar.gz
tar -xzf hugo_0.55.2_Linux-64bit.tar.gz

./hugo